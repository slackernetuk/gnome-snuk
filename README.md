# Gnome SlackerNetUK ( gsnuk ) Gnome for Slackware -current

### Introduction

 This is GNOME 42... from Scratch (for Slackware)

### Compatibilty
 
100% Compatible with slackware64-current

### Download and Install Compiled Packages
1. Download using this command:
**slackware64-current**
```bash
lftp -c mirror https://slackernet.ddns.net/slackware/slackware64-current/slackware64/gnome/42.7/ -c gsnuk-42.7-pkg64
```

2. Before we start to install GNOME we're need to remove xdg-desktop-portal-kde, of course if KDE is installed.

As root, remove:
```
slackpkg remove xdg-desktop-portal-kde
```

3. As root, install:
```bash
upgradepkg --install-new --reinstall gsnuk-42.7-pkg64/*_snuk.txz
```

### Configuring
You need to add some groups and users to make things work better (I think), so:
1. In console (root), type:
```bash
groupadd -g 214 avahi
useradd -u 214 -g 214 -c "Avahi User" -d /dev/null -s /bin/false avahi
groupadd -g 303 colord
useradd -d /var/lib/colord -u 303 -g colord -s /bin/false colord
groupadd -g 257 pcscd
useradd -u 257 -g pcscd -d /var/run/pcscd -s /bin/false pcscd
```
2. Avahi need to be run at boot, so edit your `/etc/rc.d/rc.local` adding these lines:
```bash
# Start avahidaemon
if [ -x /etc/rc.d/rc.avahidaemon ]; then
 /etc/rc.d/rc.avahidaemon start
fi
# Start avahidnsconfd
if [ -x /etc/rc.d/rc.avahidnsconfd ]; then
  /etc/rc.d/rc.avahidnsconfd start
fi
# Start power-profiles-daemon
if [ -x /etc/rc.d/rc.power-profiles-daemon ]; then
  /etc/rc.d/rc.power-profiles-daemon start 
fi
#Start pcscd daemon
if [ -x /etc/rc.d/rc.pcscd ]; then
  /etc/rc.d/rc.pcscd start
fi
#Start libvirt daemon
if [ -x /etc/rc.d/rc.libvirt ]; then
  /etc/rc.d/rc.libvirt start
fi
```
3. Also stop Avahi at shutdown, so edit your `/etc/rc.d/rc.local_shutdown` adding these lines:
```bash
# Stop avahidnsconfd
if [ -x /etc/rc.d/rc.avahidnsconfd ]; then
  /etc/rc.d/rc.avahidnsconfd stop
fi
# Stop avahidaemon
if [ -x /etc/rc.d/rc.avahidaemon ]; then
  /etc/rc.d/rc.avahidaemon stop
fi
# Stop libvirt daemon
if [ -x /etc/rc.d/rc.libvirt ]; then
  /etc/rc.d/rc.libvirt stop
fi
# Stop power-profiles-daemon
if [ -x /etc/rc.d/rc.power-profiles-daemon ]; then
  /etc/rc.d/rc.power-profiles-daemon stop 
fi
```
4. Edit your /etc/inittab to go 4 runlevel ( id:3:initdefault: -> id:4:initdefault: )
5. Make sure gdm is the first one to run in the /etc/rc.d/rc.4 (remove the -nodaemon option)
8. Reboot your system

### How to compile and use GNOME 42 on Slackware -current
 1. In console (root), type:
```bash
git clone https://gitlab.com/slackernetuk/gnome-snuk.git -b main
cd gnome-snuk
sh gsnuk
```
 2. Edit your /etc/inittab to go 4 runlevel ( id:3:initdefault: -> id:4:initdefault: )
 3. Make sure gdm is the first one to run in the /etc/rc.d/rc.4 (remove the -nodaemon option)
 4. Reboot your system
 


### Thanks
- [Slackware UK](http://slackware.uk/)
- [Linux From Scratch](http://www.linuxfromscratch.org/)
- [Arch Linux Team](https://www.archlinux.org/)
- [Gentoo Linux Team](https://www.gentoo.org/)
- [SlackBuilds Team](https://slackbuilds.org/)
- [Willy Sudiarto Raharjo](https://github.com/willysr)
- [BobF](https://github.com/0xBOBF)

### Contact.
 If you can help me to improve this project, please:
  - slackernetuk@gmail.com

I hope you enjoy it,

[Frank Honolka](https://www.facebook.com/frank.honolka.549/)

(2022, Great Britain)
